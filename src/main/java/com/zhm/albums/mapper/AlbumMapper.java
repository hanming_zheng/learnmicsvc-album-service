package com.zhm.albums.mapper;


import com.zhm.albums.data.Album;
import com.zhm.albums.dto.AlbumCreateDTO;
import com.zhm.albums.dto.AlbumResponseDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public abstract class AlbumMapper {

    @Mapping(target = "albumId", expression = "java(album.getId().toString())")
    public abstract AlbumResponseDTO albumToAlbumResponseDTO(Album album);

    @Mapping(target = "id", ignore = true)
    public abstract Album AlbumCreateDTOToAlbum(AlbumCreateDTO dto);
}
