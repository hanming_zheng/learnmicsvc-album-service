/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zhm.albums.service;

import com.zhm.albums.data.Album;
import com.zhm.albums.repo.AlbumRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class AlbumsService {

    private AlbumRepository albumRepository;

    @Autowired
    public AlbumsService(AlbumRepository albumRepository) {
        this.albumRepository = albumRepository;
    }

    public List<Album> getAlbums(String userId) {
        List<Album> returnValue = new ArrayList<>();

        returnValue = albumRepository.getAllByUserId(userId);

        return returnValue;
    }

    public Album addAlbum(Album album) {
        return albumRepository.save(album);
    }

}
