package com.zhm.albums.repo;

import com.zhm.albums.data.Album;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.UUID;

public interface AlbumRepository extends JpaRepository<Album, UUID> {
    List<Album> getAllByUserId (String userId);
}
