/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zhm.albums.controller;

import com.zhm.albums.data.Album;
import com.zhm.albums.dto.AlbumCreateDTO;
import com.zhm.albums.dto.AlbumResponseDTO;
import com.zhm.albums.mapper.AlbumMapper;
import com.zhm.albums.service.AlbumsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/albums")
public class AlbumsController {

    private AlbumsService albumsService;
    private AlbumMapper albumMapper;
    private Environment env;

    Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    public AlbumsController(AlbumsService albumsService, AlbumMapper albumMapper, Environment env) {
        this.albumsService = albumsService;
        this.albumMapper = albumMapper;
        this.env = env;
    }

    @GetMapping("/status/ping")
    public String status(){
        return "Ping on port " + env.getProperty("local.server.port") + " instance-id: " + env.getProperty("eureka.instance.instance-id");
    }

    @GetMapping("/by-user/{id}")
    public List<AlbumResponseDTO> userAlbums(@PathVariable String id) {

        List<AlbumResponseDTO> returnValue = new ArrayList<>();

        List<Album> albums = albumsService.getAlbums(id);

        if (albums == null || albums.isEmpty()) {
            return returnValue;
        }

        returnValue = albums.stream().map(albumMapper::albumToAlbumResponseDTO).collect(Collectors.toList());

        logger.info("Returning " + returnValue.size() + " albums");
        return returnValue;
    }

    @PostMapping("/by-user/{id}")
    public Album addAlbumToUser(@PathVariable String id, @Valid @RequestBody AlbumCreateDTO dto){
        Album albumToSave = albumMapper.AlbumCreateDTOToAlbum(dto);
        return albumsService.addAlbum(albumToSave);
    }
}
