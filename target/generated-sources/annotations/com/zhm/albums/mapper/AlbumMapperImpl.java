package com.zhm.albums.mapper;

import com.zhm.albums.data.Album;
import com.zhm.albums.dto.AlbumCreateDTO;
import com.zhm.albums.dto.AlbumResponseDTO;
import javax.annotation.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2020-02-02T14:07:01+0800",
    comments = "version: 1.3.1.Final, compiler: javac, environment: Java 1.8.0_241 (Oracle Corporation)"
)
@Component
public class AlbumMapperImpl extends AlbumMapper {

    @Override
    public AlbumResponseDTO albumToAlbumResponseDTO(Album album) {
        if ( album == null ) {
            return null;
        }

        AlbumResponseDTO albumResponseDTO = new AlbumResponseDTO();

        albumResponseDTO.setUserId( album.getUserId() );
        albumResponseDTO.setName( album.getName() );
        albumResponseDTO.setDescription( album.getDescription() );

        albumResponseDTO.setAlbumId( album.getId().toString() );

        return albumResponseDTO;
    }

    @Override
    public Album AlbumCreateDTOToAlbum(AlbumCreateDTO dto) {
        if ( dto == null ) {
            return null;
        }

        Album album = new Album();

        album.setUserId( dto.getUserId() );
        album.setName( dto.getName() );
        album.setDescription( dto.getDescription() );

        return album;
    }
}
